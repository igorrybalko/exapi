const Joi = require('@hapi/joi');

exports.createValidation = data => {

    const schema = Joi.object({
        manufacturer: Joi.string().max(255).required(),
        model: Joi.string().max(255).required(),
        price: Joi.number().min(0).max(10000000).required(),
    });
    return schema.validate(data);
};
