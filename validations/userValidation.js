const Joi = require('@hapi/joi');

const registerValidation = data => {

    const schema = Joi.object({
        name: Joi.string().min(2).max(255).required(),
        login: Joi.string().min(4).max(255).required().email(),
        password: Joi.string().min(4).max(255).required(),
        'password-confirm': Joi.string().valid(Joi.ref('password')).required()
    });
    return schema.validate(data);
};

const loginValidation = data => {

    const schema = Joi.object({
        login: Joi.string().min(4).required().email(),
        password: Joi.string().min(4).required(),
    });
    return schema.validate(data);
};

exports.registerValidation = registerValidation;
exports.loginValidation = loginValidation;
