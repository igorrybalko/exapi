var express = require('express');
var router = express.Router();
const multer  = require('multer');

const storageConfig = multer.diskStorage({
  destination: (req, file, cb) =>{
    cb(null, "uploads");
  },
  filename: (req, file, cb) =>{
    cb(null, file.originalname);
  }
});

const upload = multer({ storage:storageConfig });

/* GET home page. */
router.get('/', (req, res, next) => {
  res.render('index');
});

router.post('/', upload.single('avatar'), (req, res, next) => {
  let filedata = req.file;
  if(!filedata)
    res.send("Ошибка при загрузке файла");
  else
    res.send("Файл загружен");
});

module.exports = router;
