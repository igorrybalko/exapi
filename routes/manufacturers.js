const router = require('express').Router();

const ctrl = require('../controllers/ManufacturerController'),
    isAuth = require('../ext/auth');
/**
 * test
 * @route POST /orders
 * @group Orders - Create order
 */
router.post('/', isAuth, ctrl.createEntity);

/**
 * test
 * @route GET /orders/users
 * @group Orders - Users and orders
 */
router.get('/', isAuth, ctrl.getAll.bind(ctrl));

/**
 * @route GET /orders/{id}
 * @group Orders - Operations about orders
 * @param {string} id.required
 * @returns {Order.model} 200 - order info
 * @returns {Error}  default - Unexpected error
 */
router.get('/:id', isAuth, ctrl.getById.bind(ctrl));

module.exports = router;