/**
 * @typedef Product
 * @property {string} img
 * @property {string} manufacturer
 * @property {string} model
 * @property {number} price
 * @property {string} create
 * @property {string} _id
 */

const router = require('express').Router();

const ctrl = require('../controllers/ProductController'),
    isAuth = require('../ext/auth');

/**
 * test
 * @route POST /products
 * @group Products - Create product
 */
router.post('/', isAuth, ctrl.createEntity);

/**
 * @route GET /products
 * @group Products - Get all products
 */
router.get('/', isAuth, ctrl.getAll.bind(ctrl));

/**
 * @route GET /products/create
 * @group Products - Get data for create
 */
router.get('/create', isAuth, ctrl.getForEdit);

/**
 * @route GET /products/list
 * @param {string} skip.query
 * @param {string} limit.query
 * @param {string} order.query
 * @param {string} sort.query
 * @group Products - Get products
 */
router.get('/list', isAuth, ctrl.getList.bind(ctrl));

/**
 * @route GET /products/xls
 * @group Products - Get xls products
 */
router.get('/xls', isAuth, ctrl.getXls);

/**
 * @route GET /products/pdf
 * @group Products - Get xls products
 */
router.get('/pdf', isAuth, ctrl.getPDF);

/**
 * @route GET /products/{id}
 * @group Products - Operations about product
 * @param {string} id.required
 * @returns {Product.model} 200 - An array of products info
 * @returns {Error}  default - Unexpected error
 */
router.get('/:id', isAuth, ctrl.getById.bind(ctrl));


/**
 * @route GET /products/{id}/edit
 * @group Products - Operations about product
 * @param {string} id.required
 * @returns {Product.model} 200 - An array of products info
 * @returns {Error}  default - Unexpected error
 */
router.get('/:id/edit', isAuth, ctrl.getForEdit);

/**
 * @route DELETE /products/{id}
 * @group Products - Operations about products
 */
router.delete('/:id', isAuth, ctrl.deleteEntity.bind(ctrl));

/**
 * @route PUT /products/{id}
 * @group Products - Operations about products
 */
router.put('/:id', isAuth, ctrl.updateEntity.bind(ctrl));


module.exports = router;
