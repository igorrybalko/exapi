const router = require('express').Router();

const userController = require('../controllers/UserController');

/* GET users listing. */
router.get('/', (req, res, next) => {
  res.send('respond with a resource');
});

/**
 * test
 * @route POST /users/register
 * @group Users - Operations about users
 * @param {string} login.required - email
 * @param {string} name.required - user's name
 * @param {string} password.required - user's password
 * @param {string} password-confirm.required - user's password
 */
router.post('/register', userController.register);

/**
 * test
 * @route POST /users/login
 * @group Users - Operations about users
 * @param {string} login.required - email
 * @param {string} password.required - user's password
 * @returns {object} 200 - Token
 */
router.post('/login', userController.login);


module.exports = router;
