const User = require('../models/user').User,
    sanitize = require('mongo-sanitize'),
    {registerValidation, loginValidation} = require('../validations/userValidation');


function register(req, res, next) {

    const {error} = registerValidation(req.body);
    if(error) return res.status(400).json({message: error.details[0].message});

    let user = new User({
        login: sanitize(req.body.login),
        name: sanitize(req.body.name),
        password: sanitize(req.body.password)
    });

    user.save(function (err) {

        if (err) return res.status(400).json({ message: 'Error create user' , err});
        res.json({ message: 'User was created' });
        res.end();

    });

}

function login(req, res, next){

    const {error} = loginValidation(req.body);
    if(error) return res.status(400).json({message: error.details[0].message});

    User.findOne({login: sanitize(req.body.login.trim())}, function(err, user){

        if(err) return res.status(400).json({ message: 'Error auth'});

        if(user){

            if(user.checkPassword(req.body.password.trim())){

                res.json({ token: user.createToken(), name: user.name });

                res.end();
            }else{
                return res.status(400).json({ message: 'Error password'});
            }
        }else{
            return res.status(400).json({ message: 'User not found'});
        }

    });
}

exports.register = register;
exports.login = login;
