const sanitize = require("mongo-sanitize"),
  fs = require('fs'),
  pdf = require('html-pdf'),
  xl = require('excel4node'),
  path = require('path'),
  streamLength = require("stream-length");

const Product = require("../models/product").Product,
  Manuf = require("../models/manufacturer"),
  BaseController = require("./BaseController"),
  { createValidation } = require("../validations/productValidation");

class ProductController extends BaseController {
  constructor(model) {
    super(model);
  }

  createEntity(req, res, next) {
    const { error } = createValidation(req.body);
    if (error)
      return res.status(400).json({ message: error.details[0].message });

    let product = new Product({
      manufacturer: sanitize(req.body.manufacturer),
      model: sanitize(req.body.model),
      price: sanitize(req.body.price)
    });

    product.save(function(err, data) {
      if (err) {
        return res.status(400).json({ message: "Error create product", err });
      }
      res.json({ message: "Product was created", id: data._id });
      res.end();
    });
  }

  async getForEdit(req, res, next) {

    try{

      let content = {};
      if(req.url != '/create'){
        content = await Product.findById(sanitize(req.params.id), '-__v');
      }

      res.json({
        content,
        manufs: await Manuf.find({}, '-__v -create')
      });
      res.end();

    }catch(err){
      res.status(400).json({ message: "Error get", err });
    }
  }

  async getById(req, res, next) {
    try {
      let prod = await Product.findById(sanitize(req.params.id), '-__v'),
          manuf = await Manuf.findById(sanitize(prod.manufacturer));

      prod.manufacturer = manuf.title;

      res.json(prod);
      res.end();

    }catch(err){
      res.status(400).json({ message: "Error get", err });
    }

  }

  async getList(req, res, next) {

    let {limit, skip, page, total} = await this.getPaginParams(req),
        sort = sanitize(req.query.sort),
        order = parseInt(req.query.order);

    let query = Product.aggregate([
      {
        $lookup: {
          from: "manufacturers",
          let: {manuf: "$manufacturer"},
          pipeline: [
            {$addFields: {manuf: {$toString: "$_id"}}},
            {$match: {$expr: {$eq: ["$manuf", "$$manuf"]}}},
            {
              $project: {
                title: 1,
              }
            }
          ],
          as: "manuf"
        }
      },
      {
        $project: {
          _id: 1,
          create: 1,
          model: 1,
          price: 1,
          manuf: { "$arrayElemAt": [ "$manuf", 0 ] }
        }
      }
    ]);

    if(order && sort){
      query.sort({[sort]: order});
    }

    query.skip(skip).limit(limit).exec((err, data) => {
      if (err)
        return res.status(400).json({message: "Error get list", err});

      res.json({
        pagination: {
          total,
          page
        },
        content: data
      });
      res.end();
    });
  }

  getXls(req, res, next){
    let wb = new xl.Workbook();
    let ws = wb.addWorksheet('Sheet 1');

    let style = wb.createStyle({
      font: {
        color: '#000000',
        size: 12,
      },
      numberFormat: '$#,##0.00; ($#,##0.00); -',
    });

    ws.cell(1, 1).string('Model').style(style);
    ws.cell(1, 2).string('Manufacturer').style(style);
    ws.cell(1, 3).string('Price').style(style);

    let ind = 2;

    Product.find({}, (err, data) => {
      if (err)
        return res.status(400).json({ message: "Error get excel", err });

        data.forEach((el, i) => {

          ws.cell(i + ind, 1).string(el.model).style(style);
          ws.cell(i + ind, 2).string(el.manufacturer).style(style);
          ws.cell(i + ind, 3).number(el.price).style(style);

        });

      wb.write('ExcelFile.xlsx', res);
    });

  }
  getPDF(req, res, next){

    let html = fs.readFileSync(__dirname + '/../views/pdf/example.html', 'utf8');

      pdf.create(html).toBuffer(function(err, buffer){

      if (err)
        return res.status(400).json({ message: "Error get pdf", err });

      res.setHeader('Content-Type', 'application/pdf');
      res.setHeader('Content-Disposition', 'attachment; filename=quote.pdf');


      res.send(buffer);

      res.end();
    });

  }

}

module.exports = new ProductController(Product);
