const sanitize = require("mongo-sanitize"),
  db = require("mongoose"),
  jwt = require("jsonwebtoken");

const Order = require("../models/order").Order,
  Product = require("../models/product").Product,
  BaseController = require("./BaseController");

class OrderController extends BaseController {
  constructor(model) {
    super(model);
  }

  createEntity(req, res, next) {
    let token = req.headers.authorization.substr(7),
      decoded = jwt.decode(token),
      productIds = req.body.map(el => db.Types.ObjectId(sanitize(el.product)));

    Product.find(
      {
        _id: { $in: productIds }
      },
      (err, prods) => {
        let prices = {};
        prods.forEach(el => {
          prices[el._id] = el.price;
        });

        let totalPrice = 0;
        req.body.forEach(el => {
          totalPrice += prices[el.product] * el.quantity;
        });

        let order = new Order({
          userId: decoded.id,
          products: sanitize(req.body),
          totalPrice
        });

        order.save(err => {
          if (err) {
            return res.status(400).json({ message: "Error create order", err });
          }
          res.json({ message: "Order was created" });
          res.end();
        });
      }
    );
  }

  getUsersAndOrders(req, res, next) {
    Order.aggregate([
      {
        $lookup: {
          from: "users",
          let: { userId: "$userId" },
          pipeline: [
            { $addFields: { userId: { $toString: "$_id" } } },
            { $match: { $expr: { $eq: ["$userId", "$$userId"] } } },
            {
              $project: {
                _id: 1,
                name: 1,
                login: 1
              }
            }
          ],
          as: "orderdetails"
        }
      },
      {
        $project: {
          _id: 1,
          create: 1,
          totalPrice: 1,
          products: 1,
          orderdetails: 1
        }
      }
    ]).exec((err, data) => {
      if (err) return res.status(400).json({ message: "Error", err });

      res.json(data);
      res.end();
    });
  }
}

module.exports = new OrderController(Order);
