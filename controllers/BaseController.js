const sanitize = require("mongo-sanitize");

class BaseController {
  constructor(model) {
    this.model = model;
  }

  getById(req, res, next) {
    this.model.findById(sanitize(req.params.id), (err, data) => {
      if (err) return res.status(400).json({ message: "Error get", err });

      res.json(data);
      res.end();
    });
  }

  deleteEntity(req, res, next) {
    this.model.findByIdAndRemove(sanitize(req.params.id), (err, data) => {
      if (err) return res.status(400).json({ message: "Error delete", err });

      res.json({
        message: "Deleted"
      });
      res.end();
    });
  }

  updateEntity(req, res, next) {
    let reqBody = {};
    for (let key in req.body) {
      reqBody[key] = sanitize(req.body[key]);
    }

    this.model.findByIdAndUpdate(sanitize(req.params.id), reqBody, (err, data) => {
      if (err) return res.status(400).json({ message: "Error update", err });

      res.json({
        message: "Updated",
        id: data._id
      });
      res.end();
    });
  }

  getPaginParams(req){

    return new Promise((resolve, reject) => {

      this.model.countDocuments({}, (err, total) => {
        if(err) return reject(err);

        let limit = parseInt(req.query.limit);
        if (limit < 3) {
          limit = 3;
        }

        let page = parseInt(req.query.page);
        if (page < 1) {
          page = 1;
        }

        let skip = (page - 1) * limit;

        resolve({limit, skip, page, total})
      });

    });


  }

  async getList(req, res, next) {

    let { limit, skip, page, total } = await this.getPaginParams(req);

    this.model
      .find({})
      .skip(skip)
      .limit(limit)
      .exec((err, data) => {
        if (err)
          return res.status(400).json({ message: "Error get list", err });

        res.json({
          pagination: {
            total,
            page
          },
          content: data
        });
        res.end();
      });
  }

  getAll(req, res, next) {
    this.model.find({}, (err, data) => {
      if (err)
        return res.status(400).json({ message: "Error get all", err });

      res.json(data);
      res.end();
    });
  }
}

module.exports = BaseController;
