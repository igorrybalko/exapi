const conf = require('../config'),
    db = require('mongoose')

db.set('useNewUrlParser', true)
db.set('useFindAndModify', false)
db.set('useCreateIndex', true)
db.set('useUnifiedTopology', true)

db.connect(conf.get('db-cnn'))
module.exports = db
