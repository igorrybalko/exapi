const jwt = require('jsonwebtoken'),
    conf = require('../config')

function isAuth(req, res, next) {

    let auth = req.headers.authorization;

    if( auth && auth.startsWith('Bearer ')){
       let token = auth.substr(7);

        if (jwt.verify(token, conf.get('secretJwt')))
            return next()
        else{
            return res.status(401).json({ message: 'Unauthorized'})
        }
    }
    return res.status(401).json({ message: 'Unauthorized'})
}

module.exports = isAuth
