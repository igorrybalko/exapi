const bcrypt = require('bcryptjs'),
    jwt = require('jsonwebtoken'),
    conf = require('../config');

let db = require('../ext/db');

let schema = new db.Schema({
    login: {
        type: String,
        required: true,
        unique: true,
        maxlength: 255,
        minlength: 4,
        trim: true
    },
    name: {
        type: String,
        required: true,
        maxlength: 255,
        minlength: 2,
        trim: true
    },
    hash: {
        type: String,
        required: true
    },
    saltRounds: {
        type: Number,
        required: true
    },
    create: {
        type: Date,
        default: Date.now()
    }
});

schema.virtual('password').set(function (password) {

        this.saltRounds = parseInt(Math.random() * 10 + 1);
        let salt = bcrypt.genSaltSync(this.saltRounds);
        this.hash = bcrypt.hashSync(password, salt);

        // bcrypt.genSalt(this.saltRounds, (err, salt) => {
        //     bcrypt.hash(password, salt, (err, hash) => {
        //         this.hash = hash;
        //         console.log(this);
        //     });
        // });

    })
    .get(() => {
        return this.hash;
    });

schema.methods.checkPassword = function (password) {
    return bcrypt.compareSync(password, this.hash);
};

schema.methods.createToken = function() {
   return jwt.sign({name: this.name, id: this._id},
        conf.get('secretJwt'),
        { expiresIn: '24h'}
    );
};

exports.User = db.model('User', schema);
