let db = require('../ext/db');

let schema = new db.Schema({
    userId: {
        type: String,
        maxlength: 255,
        required: true,
    },
    products: {
        type: [db.Schema.Types.Mixed],
        required: true,
    },
    totalPrice: {
        type: Number,
        required: true
    },
    create: {
        type: Date,
        default: Date.now()
    }
});

exports.Order = db.model('Order', schema);
