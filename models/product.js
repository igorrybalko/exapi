let db = require('../ext/db');

let schema = new db.Schema({
    img: {
        type: String,
        maxlength: 255
    },
    manufacturer: {
        type: String,
        required: true,
        maxlength: 255,
    },
    model: {
        type: String,
        required: true,
        maxlength: 255,
        trim: true
    },
    price: {
        type: Number,
        required: true,
        max: 100000000
    },
    produced: {
        type: Date
    },
    create: {
        type: Date,
        default: Date.now()
    }
});

exports.Product = db.model('Product', schema);
