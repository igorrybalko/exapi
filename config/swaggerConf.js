let conf = require('./index');

let swaggerConf = {
    swaggerDefinition: {
        info: {
            description: 'This is a sample server',
            title: 'Swagger',
            version: '1.0.0',
        },
        host: conf.get('domain') + ':' + conf.get('port'),
        basePath: '/',
        produces: [
            "application/json",
            "application/xml"
        ],
        schemes: ['http', 'https'],
        securityDefinitions: {
            JWT: {
                type: 'apiKey',
                in: 'header',
                name: 'Authorization',
                description: "",
            }
        }
    },
    basedir: '', //app absolute path
    files: ['./routes/**/*.js'] //Path to the API handle folder
};

module.exports = swaggerConf;
