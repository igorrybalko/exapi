module.exports = {
  "port": 3001,
  "domain": "localhost",
  "app-view": "views",
  "app-engine": "ejs",
  "app-static": "public",
  "db-cnn": process.env.DB_CNN,
  "secretJwt": process.env.SECRET_JWT,
  "session": {
    "secret": "secretWorld",
    "key": "NODESESSID",
    "cookie": {
      "path": "/",
      "maxAge": null,
      "httpOnly": true
    }
  },
  "cors": ["http://localhost:3002"]
};
