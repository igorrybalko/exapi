require('dotenv').config();

const createError = require('http-errors'),
  express = require('express'),
  path = require('path'),
  cookieParser = require('cookie-parser'),
  logger = require('morgan'),
  helmet = require('helmet'),
  fs = require('fs'),
  swaggerConf = require('./config/swaggerConf'),
  conf = require('./config'),
  cors = require('cors');

const app = express();
const expressSwagger = require('express-swagger-generator')(app);

expressSwagger(swaggerConf);

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', conf.get('app-engine'));

app.use(helmet());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, conf.get('app-static'))));

let corsOptions = {
  origin(origin, callback) {
    if (conf.get('cors').includes(origin)) {
      callback(null, true)
    } else {
      callback(new Error('Not allowed by CORS'))
    }
  }
};

app.use(cors());

fs.readdirSync('./routes/').forEach(file => {
  let fileName = file.substr(0, file.length - 3);
  app.use(fileName == 'index' ? '/' : '/' + fileName, require('./routes/' + fileName));
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  let status = err.status || 500;
  res.status(status).json({ message: 'ERROR ' , err});
  res.end();
});

module.exports = app;
