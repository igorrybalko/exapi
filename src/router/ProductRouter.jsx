import {Route, Switch} from "react-router-dom"
import React, {lazy} from "react"

import ProductCreatePage from "../views/product/ProductCreatePage"
import ProductEditPage from "../views/product/ProductEditPage"
import ProductPage from "../views/product/ProductPage"
const CatalogPage = lazy(() => import("../views/product/CatalogPage"))

export default function RouterOutlet(){
    return (
        <Switch>
            <Route path="/catalog/:id/edit/" component={ProductEditPage} />
            <Route path="/catalog/create" component={ProductCreatePage} />
            <Route path="/catalog/:id" component={ProductPage} />
            <Route path="/catalog" component={CatalogPage} />
        </Switch>

    );
}