import React, { Suspense, lazy }  from 'react'
import {Route, Switch} from 'react-router-dom'

import LoginPage from '../views/user/LoginPage'
import ProductRouter from './ProductRouter'
import UsersPage from '../views/user/UsersPage'

export default function RouterOutlet(){
    return (
        <Suspense fallback={<div>Загрузка...</div>}>
            <Switch>
                <Route path="/login" component={LoginPage} />
                <ProductRouter />
                <Route path="/users" component={UsersPage} />
            </Switch>
        </Suspense>
    );
}
