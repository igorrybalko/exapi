export function getState(key) {
    const state = JSON.parse(localStorage.getItem('persist:root'));
    if(state) return JSON.parse(state[key]);
    return {};
}
