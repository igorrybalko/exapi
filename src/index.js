import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter } from "react-router-dom"
import axios from 'axios'
import { Provider } from 'react-redux'
import 'antd/dist/antd.css'
import { PersistGate } from 'redux-persist/integration/react'

import './scss/main.scss'
import App from './App'
import {store, persistor} from './store'
import {getState} from './middleware/appHelper'

axios.defaults.headers.common[ 'Accept' ] = 'application/json';
axios.defaults.baseURL = process.env.REACT_APP_BASE_URL;

let token = getState('user').token;
if(token){
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
}

const UNAUTHORIZED = 401;
axios.interceptors.response.use(
    response => response,
    error => {

        console.log(error);
        if(error.response){
            const {status} = error.response;
            if (status === UNAUTHORIZED) {

                window.history.push('/login');
            }
        }

        return Promise.reject(error);
    }
)

ReactDOM.render(
    <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
            <BrowserRouter>
                <App />
            </BrowserRouter>
        </PersistGate>
    </Provider>,
    document.getElementById('root'));
