const conf = {
    limit: 5,
    url: {
        product: {
            list: params => `products/list?page=${params.page}&limit=${conf.limit}&sort=${params.sort}&order=${params.order}`,
            item: id => `products/${id}`,
            edit: id => `products/${id}/edit`,
            create: 'products/create',
            save: 'products',
            xls: 'products/xls',
            pdf: 'products/pdf'
        }
    }
}

export default conf;
