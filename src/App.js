import React from 'react';
import { Layout} from 'antd';
import RouterOutlet from './router/RouterOutlet';
import AppHeader from "./components/common/AppHeader";

const { Content, Footer } = Layout;

function App() {

    function some(arg) {
       // console.log(arg);
    }

  return (
      <Layout>
          <AppHeader some={some}></AppHeader>
          <Content>
              <div style={{ background: '#fff', padding: 24, minHeight: 380 }}>
                  <RouterOutlet/>
              </div>
          </Content>
          <Footer>footer</Footer>
      </Layout>
  );
}

export default App;
