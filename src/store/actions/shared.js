import { notification } from 'antd'
import axios from "axios"

/*
 * action types
 */
export const SHOW_NOTIFICATION = 'SHOW_NOTIFICATION'
export const GET_PAGE_DATA = 'GET_PAGE_DATA'

export function showNotification(payload) {

    let description = payload.data.desc;

    if(payload.type == 'error'){

        description = payload.data.err ? payload.data.err.message : payload.data.message
    }

    notification[payload.type]({
        message: payload.data.message,
        description
    });

    return { type: SHOW_NOTIFICATION, payload }
}

export function getPageData(url) {
    return (dispatch) => {
        axios.get(url)
            .then(res => {
                dispatch({type: GET_PAGE_DATA, payload: res.data});
            }).catch(err => {
                dispatch({type: SHOW_NOTIFICATION, payload: {type: 'error', desc: err.response.data}});
        });
    }
}

export function changePageData(payload) {
    return { type: GET_PAGE_DATA, payload }
}