import axios from 'axios'

/*
 * action types
 */
export const LOGIN = 'LOGIN'
export const LOGOUT = 'LOGOUT'
export const REGISTER = 'REGISTER'

// export function login(payload) {
//     return { type: LOGIN, payload }
// }

export function login(payload) {
    return (dispatch) => {
        axios.post('users/login', payload)
            .then(res => {

                //let now = Date.now();

                // this.$store.dispatch('setAuthTime', now);

                axios.defaults.headers.common['Authorization'] = 'Bearer ' + res.data.token;
                dispatch({type: LOGIN, payload: res.data});
                this.history.push('/catalog');


            }).catch(error => {
                console.log(error);
        });
    }
}

