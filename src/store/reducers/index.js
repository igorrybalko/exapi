import { combineReducers } from 'redux'
import userReducer from './user'
import sharedReducer from './shared'

export default combineReducers({
    user: userReducer,
    shared: sharedReducer
})
