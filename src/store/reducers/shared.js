import {GET_PAGE_DATA} from '../actions/shared'

const initState = {
    pd: {}
};

const sharedReducer = (state = initState, action) => {
    switch (action.type) {
        case GET_PAGE_DATA:
            return {
                ...state,
                pd: action.payload}
        default:
            return state
    }
};

export default sharedReducer;
