import {LOGIN} from '../actions/users'

const initState = {
    token: '',
    name: ''
}

const userReducer = (state = initState, action) => {
    switch (action.type) {
        case LOGIN:
            return {...state,
                token: action.payload.token,
                name: action.payload.name}
        default:
            return state
    }
};

export default userReducer;
