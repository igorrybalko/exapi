import React from 'react'
import { connect } from 'react-redux'
import { Form, Input, Button } from 'antd'
import { bindActionCreators } from 'redux'

import {login as loginAction} from '../../store/actions/users'

class LoginPage extends React.Component{

    constructor(props) {
        super(props);

        this.state = {
            form: {
                login: '',
                password: ''
            }
        }
    }

    handleSubmit = () => {
        this.props.loginAction(this.state.form);
    };

    change(e) {
        let form = {...this.state.form};
        form[e.target.name] = e.target.value;
        this.setState({form});
    };

    render() {
        return (
            <div>
                <Form layout="vertical" onFinish={this.handleSubmit}>
                    <Form.Item>
                        <Input placeholder="login" name="login" value={this.state.form.login} onChange={this.change.bind(this)} />
                    </Form.Item>
                    <Form.Item>
                        <Input.Password placeholder="password" name="password" value={this.state.form.password} onChange={this.change.bind(this)} />
                    </Form.Item>
                    <Form.Item>
                        <Button type="primary" htmlType="submit">
                            Submit
                        </Button>
                    </Form.Item>
                </Form>
            </div>
        );
    }

}

function mapDispatchToProps(dispatch) {
    return {
        loginAction: bindActionCreators(loginAction, dispatch)
    }
}

export default connect(null, mapDispatchToProps)(LoginPage)
