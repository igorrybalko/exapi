import React from 'react'
import {connect} from "react-redux"
import axios from "axios"
import { Table, Pagination, Button } from 'antd'
import { DownloadOutlined } from '@ant-design/icons'
import {Link} from "react-router-dom";
import {bindActionCreators} from "redux"

import * as sharedActions from "../../store/actions/shared"
import conf from "../../config"


function mapStateToProps (state) {
    return {
        user: state.user
    }
}

function mapDispatchToProps(dispatch) {
    return {
        sharedActions: bindActionCreators(sharedActions, dispatch)
    }
}

class CatalogPage extends React.Component{

    constructor(props) {
        super(props);

        this.state = {
            columns: [
                {
                    title: 'model',
                    dataIndex: 'model',
                    key: 'model',
                    sorter: true,
                    render: (text, item) => <Link to={'/catalog/' + item._id}>{text}</Link>,
                },
                {
                    title: 'manufacturer',
                    dataIndex: 'manuf',
                    key: 'manuf',
                    render: (el, item) => <div>{el.title}</div>,
                    width: '40%',
                    sorter: true,
                },
                {
                    title: 'price',
                    dataIndex: 'price',
                    key: 'price',
                    sorter: true,
                },
            ],
            pd: {
                pagination: {},
            },
            req: {
                sort: '',
                order: '',
                page: 1
            },
            href: ''
        }
    }

    componentWillMount() {
        this.doRequest();
    }

    doRequest(){

        let orderType = {
            ascend: 1,
            descend: -1
        };

        let req = {
            page: this.state.req.page,
            order: orderType[this.state.req.order] || '',
            sort: this.state.req.sort
        };

        axios.get(conf.url.product.list(req))
            .then(res => {
                this.setState({pd: res.data});
        }).catch(error => {
            this.props.sharedActions.showNotification({type: 'error', data: error.response ? error.response.data : 'error'});
        });
    }

    paginate(page){
        let req = {
            ...this.state.req,
            page
        };
        this.setState({req}, this.doRequest);
    }

    handleTableChange(pagination, filters, sorter){
        let req = {
            ...this.state.req,
            order: sorter.order,
            sort: sorter.field
        };
        this.setState({req}, this.doRequest);
    }

    downloadXls(){
        axios({
            url: conf.url.product.xls,
            responseType: 'blob',
        }).then(res => {

            let fileData = window.URL.createObjectURL(new Blob([res.data]));

            this.setState({href: fileData});

            setTimeout(() => {
                document.getElementById('downloadXls').click();
            }, 50);

            }).catch(error => {
                this.props.sharedActions.showNotification(
                    {
                        type: 'error',
                        data: error.response ? error.response.data : 'error'
                    });
        });
    }

    downloadPDF(){
        axios({
            url: conf.url.product.pdf,
            responseType: 'blob',
        }).then(res => {

            let fileData = window.URL.createObjectURL(new Blob([res.data]));

            this.setState({href: fileData});

            setTimeout(() => {
                document.getElementById('downloadPDF').click();
            }, 50);

        }).catch(error => {
            this.props.sharedActions.showNotification(
                {
                    type: 'error',
                    data: error.response ? error.response.data : 'error'
                });
        });
    }

    render() {
        return (
            <div>
                <Table columns={this.state.columns}
                       dataSource={this.state.pd.content}
                       pagination={false}
                       rowKey="_id"
                       onChange={this.handleTableChange.bind(this)}
                />
                <Pagination total={this.state.pd.pagination.total} defaultPageSize={conf.limit} onChange={this.paginate.bind(this)}/>
                <div className="btns-list">
                    <Link to="/catalog/create" className="ant-btn ant-btn-primary">Create</Link>
                    <Button type="primary" icon={<DownloadOutlined />} onClick={this.downloadXls.bind(this)}>
                        Download XLS
                    </Button>
                    <Button type="primary" icon={<DownloadOutlined />} onClick={this.downloadPDF.bind(this)}>
                        Download PDF
                    </Button>
                    <a id="downloadXls" href={this.state.href} download="file.xlsx"></a>
                    <a id="downloadPDF" href={this.state.href} download="file.pdf"></a>
                </div>
            </div>
        )
    }

}

export default connect(mapStateToProps, mapDispatchToProps)(CatalogPage)
