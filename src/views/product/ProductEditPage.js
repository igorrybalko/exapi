import React from 'react'

import EditCreateProduct from '../../components/product/EditCreateProduct'

export default function ProductEditPage() {
    return <EditCreateProduct method="put"></EditCreateProduct>
}