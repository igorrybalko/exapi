import React, { useState, useEffect } from 'react';
import { Row, Col, Button } from 'antd';
import axios from "axios"
import conf from "../../config";
import {bindActionCreators} from "redux";
import * as sharedActions from "../../store/actions/shared";
import {connect} from "react-redux";
import {Link} from "react-router-dom";


function mapDispatchToProps(dispatch) {
    return {
        sharedActions: bindActionCreators(sharedActions, dispatch)
    }
}

function ProductPage(props) {

    const [pd, setPd] = useState({});

    useEffect(() => {

        doRequest();

    }, []);

    const doRequest = () => {

        axios.get(conf.url.product.item(props.match.params.id))
            .then(res => {
                setPd(res.data);
            }).catch(err => {

                props.sharedActions.showNotification({type: 'error', data: err.response.data});

        });

    };

    return (
        <div>
            <Row>
                <Col span={24}>
                    <h1 className="text-center">{pd.model}</h1>
                </Col>
            </Row>
            <Row>
                <Col span={8}>

                </Col>
                <Col span={16}>
                    <ul>
                        <li><b>Manufacturer:</b> {pd.manufacturer}</li>
                        <li><b>Model:</b> {pd.model}</li>
                        <li><b>Price:</b> {pd.price}</li>
                    </ul>
                </Col>
            </Row>
            <Row>
                <Col span={24}>
                    <div className="btns-list">
                        <Link to={'/catalog/' + props.match.params.id + '/edit'} className="ant-btn ant-btn-primary">Edit</Link>
                        <Button type="danger">Delete</Button>
                    </div>
                </Col>
            </Row>
        </div>
    )
}

export default connect(null, mapDispatchToProps)(ProductPage)