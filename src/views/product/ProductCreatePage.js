import React from 'react'

import EditCreateProduct from '../../components/product/EditCreateProduct'

export default function ProductCreatePage() {
    return <EditCreateProduct method="post"></EditCreateProduct>
}