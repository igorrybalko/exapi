import React, {useEffect, useState} from "react";
import { Row, Col, Button, Form, Input, InputNumber, Select } from "antd";
import conf from "../../config";
import { bindActionCreators } from "redux";
import axios from "axios";
import { connect } from "react-redux";
import { useParams, useHistory } from "react-router-dom";
import PropTypes from "prop-types";

import * as sharedActions from "../../store/actions/shared";

let { Option } = Select;

function mapDispatchToProps(dispatch) {
  return {
    sharedActions: bindActionCreators(sharedActions, dispatch)
  };
}

function mapStateToProps(state) {
  return {
    pd: state.shared.pd
  };
}

function EditCreateProduct(props) {

  const [some, setSome] = useState(0);

  let { id } = useParams(),
      history = useHistory(),
    params = {
      put: {
        saveUrl: conf.url.product.item(id),
        getUrl: conf.url.product.edit(id)
      },
      post: {
        saveUrl: conf.url.product.save,
        getUrl: conf.url.product.create
      }
    };

  useEffect(() => {
      props.sharedActions.getPageData(params[props.method].getUrl);

  }, []);

  function onFinish() {

        axios({
          method: props.method,
          url: params[props.method].saveUrl,
          data: props.pd.content
        })
          .then(res => {
            props.sharedActions.showNotification({
              type: "success",
              data: { message: "Success", desc: "Saved" }
            });

            history.push("/catalog/" + res.data.id);
          })
          .catch(err => {
            props.sharedActions.showNotification({
              type: "error",
              data: err.response.data
            });
          });
  }

  const onFinishFailed = errorInfo => {
    console.log('Failed:', errorInfo);
  };

  function change(e, type) {
    let updatedData;

    switch (type) {
      case "manufacturer":
      case "price":
        updatedData = {
          ...props.pd.content,
          [type]: e
        };
        break;
      default:
        updatedData = {
          ...props.pd.content,
          [e.target.name]: e.target.value
        };
    }

    props.sharedActions.changePageData({
      ...props.pd,
      content: updatedData
    });
  }
  if (props.pd && props.pd.content) {
    return (
      <div>
        <Form layout="vertical"
              onFinish={onFinish}
              onFinishFailed={onFinishFailed}
              initialValues={{
                manufacturer: props.pd.content.manufacturer,
                model: props.pd.content.model
              }}
        >
          <Row>
            <Col span={24}>
              <h1 className="text-center">{props.pd.content.model}</h1>
            </Col>
          </Row>
          <Row>
            <Col span={8}>{some}</Col>
            <Col span={16}>
              <div>
                <div>
                  <Form.Item label="Manufacturer:" rules={[{ required: true, message: 'Please input manufacturer!' }]} name="manufacturer">
                      <Select name="manufacturer"
                              onChange={e => {
                        change(e, "manufacturer");
                      }} >
                        {props.pd.manufs.map(item => (
                          <Option key={item._id} value={item._id}>
                            {item.title}
                          </Option>
                        ))}
                      </Select>
                  </Form.Item>
                </div>
                <div>
                  <Form.Item label="Model:"  name="model" rules={[{ required: true, message: 'Please input model!' }]}>
                      <Input
                        onChange={change}
                        placeholder="model"
                        name="model"
                      />
                  </Form.Item>
                </div>
                <div>
                  <Form.Item label="Price:">
                    <InputNumber
                      value={props.pd.content.price}
                      min={1}
                      onChange={e => {
                        change(e, "price");
                      }}
                      placeholder="price"
                      name="price"
                    />
                  </Form.Item>
                </div>
                {/*<li>*/}
                {/*  <Form.Item label="Manufacturer:">*/}
                {/*    <Select onChange={e => {*/}
                {/*      change(e, "manufacturer");*/}
                {/*    }} name="manufacturer" defaultValue={props.pd.content.manufacturer}>*/}
                {/*      {props.pd.manufs.map(item => (*/}
                {/*        <Option key={item._id} value={item._id}>*/}
                {/*          {item.title}*/}
                {/*        </Option>*/}
                {/*      ))}*/}
                {/*    </Select>*/}
                {/*  </Form.Item>*/}
                {/*</li>*/}
              </div>
            </Col>
          </Row>
          <Row>
            <Col span={24}>
              <Form.Item>
                <div className="btns-list">
                  <Button type="danger" htmlType="submit">
                    Save
                  </Button>
                </div>
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </div>
    );
  }
  return <div>Загрузка...</div>;
}

EditCreateProduct.propTypes = {
  method: PropTypes.string
};

export default connect(mapStateToProps, mapDispatchToProps)(EditCreateProduct);
