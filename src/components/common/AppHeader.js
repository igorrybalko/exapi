import { Menu, Layout } from "antd";
import React, {useEffect} from "react";
import { Link } from "react-router-dom";

const { Header } = Layout;

export default function AppHeader(props) {

  useEffect(() => {

   props.some('test');

  }, []);

  return (
    <Header className="header">
      <Menu theme="dark" mode="horizontal">
        <Menu.Item>
          <Link to="/catalog">Catalog</Link>
        </Menu.Item>
        <Menu.Item>
          <Link to="/users">Users</Link>
        </Menu.Item>
      </Menu>
    </Header>
  );
}
